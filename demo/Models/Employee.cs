﻿using System;
using System.Collections.Generic;

namespace demo.Models;

public partial class Employee
{
    public int Id { get; set; }

    public string? NameE { get; set; }

    public string? Gender { get; set; }

    public string? Departmente { get; set; }
}
